package ru.t1.zkovalenko.tm.exception.user;

import ru.t1.zkovalenko.tm.exception.AbstractException;

public final class ExistsLoginException extends AbstractUserException {

    public ExistsLoginException() {
        super("Login already exists");
    }

}
