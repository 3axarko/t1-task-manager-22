package ru.t1.zkovalenko.tm.command;

import ru.t1.zkovalenko.tm.api.model.ICommand;
import ru.t1.zkovalenko.tm.api.service.IServiceLocator;
import ru.t1.zkovalenko.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public abstract void execute();

    public abstract String getName();

    public abstract String getArgument();

    public abstract String getDescription();

    public abstract Role[] getRoles();

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        final Integer spacesArgs = 7;
        final Integer spacesNames = 40;
        final String argument = getArgument();
        final String name = getName();
        final String description = getDescription();
        String result = "";
        if (argument != null && !argument.isEmpty()) result += argument;
        result += spaceCounter(argument, spacesArgs);
        if (name != null && !name.isEmpty()) result += name;
        result += spaceCounter(name, spacesNames);
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

    private String spaceCounter(String string, final Integer maxSpace) {
        if (string == null) string = "";
        Integer resultCountSpaces = maxSpace - string.length();
        resultCountSpaces = resultCountSpaces > 0 ? resultCountSpaces : 1;
        String resultSpaces = "";
        for (int i = 0; i < resultCountSpaces; i++) {
            resultSpaces += " ";
        }
        return resultSpaces;
    }

    public String getUserId() {
        return serviceLocator.getAuthService().getUserId();
    }

}
