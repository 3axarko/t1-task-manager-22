package ru.t1.zkovalenko.tm.command.user;

import ru.t1.zkovalenko.tm.enumerated.Role;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

public class UserLockCommand extends AbstractUserCommand {

    private final String NAME = "user-lock";

    private final String DESCRIPTION = "User lock";

    @Override
    public void execute() {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN: ");
        final String login = TerminalUtil.nextLine();
        getUserService().lockUserByLogin(login);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
