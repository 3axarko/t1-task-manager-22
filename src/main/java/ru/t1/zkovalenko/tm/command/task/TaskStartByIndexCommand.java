package ru.t1.zkovalenko.tm.command.task;

import ru.t1.zkovalenko.tm.enumerated.Status;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

public class TaskStartByIndexCommand extends AbstractTaskCommand {

    public static final String NAME = "task-start-by-index";

    public static final String DESCRIPTION = "Task start by index";

    @Override
    public void execute() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = getUserId();
        getTaskService().changeTaskStatusByIndex(userId, index, Status.IN_PROGRESS);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
