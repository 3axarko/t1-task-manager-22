package ru.t1.zkovalenko.tm.command.system;

import ru.t1.zkovalenko.tm.command.AbstractCommand;

import java.util.Collection;

public class ApplicationHelpCommand extends AbstractSystemCommand {

    public static final String NAME = "help";

    public static final String ARGUMENT = "-h";

    public static final String DESCRIPTION = "App commands";

    @Override
    public void execute() {
        System.out.println("[HELP]");
        System.out.println("\nType key or command name\n");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) System.out.println(command);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
